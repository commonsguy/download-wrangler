package com.commonsware.android.download

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

sealed class MainViewState {
  object Loading : MainViewState()
  data class Content(val titles: List<String>) : MainViewState()
  object Error : MainViewState()
}

class MainMotor(private val repo: DownloadRepository) : ViewModel() {
  private val _states = MutableLiveData<MainViewState>()
  val states: LiveData<MainViewState> = _states

  fun download(url: String, filename: String, mimeType: String) {
    _states.value = MainViewState.Loading

    viewModelScope.launch(Dispatchers.Main) {
      try {
        repo.download(url, filename, mimeType)
        refresh()
      } catch (t: Throwable) {
        Log.e("DownloadWrangler", "Exception in downloading", t)
        _states.value = MainViewState.Error
      }
    }
  }

  fun refresh() {
    _states.value = MainViewState.Loading

    viewModelScope.launch(Dispatchers.Main) {
      try {
        _states.value = MainViewState.Content(repo.listTitles() ?: listOf())
      } catch (t: Throwable) {
        Log.e("DownloadWrangler", "Exception in listing downloads", t)
        _states.value = MainViewState.Error
      }
    }
  }
}